#ifndef NEURON_H
#define NEURON_H

#define DEFAULT_ACTIVATION_FUNCTION RELU

/* Types of neuron's activation function */
typedef enum {
	CN_ACT_STEP,
	CN_ACT_LINEAR,
	CN_ACT_SIGMOID,
	CN_ACT_RELU,
	CN_ACT_RELU_LEAKY
} cn_activation_t;

typedef struct neuron_t neuron_t;

typedef struct connection_t {
	neuron_t *endpoint;
	float value;
	float weigth;
} connection_t;

typedef struct neuron_t {
	/* Inputs */
	unsigned int input_n;
	connection_t *input;

	/* Outputs */
	unsigned int output_n;
	connection_t *output;

	/* Activation type and function */
	cn_activation_t activation_type;
	float (*activation) (neuron_t *);

	float sum;
	float threshold;
	float bias;
	float out;
} neuron_t;

int cn_neuron_new (neuron_t *new, cn_activation_t type, unsigned int in, unsigned int out, float trs, float bias);
void cn_neuron_free (neuron_t *neuron);
neuron_t *cn_neuron_array_new (unsigned int num, cn_activation_t type, unsigned int in, unsigned int out, float trs, float bias);
float cn_neuron_sum (neuron_t *neuron);
unsigned int cn_get_input_num (neuron_t *neuron);
unsigned int cn_get_output_num (neuron_t *neuron);
cn_activation_t cn_get_activation_type (neuron_t *neuron);
float cn_get_sum (neuron_t *neuron);
float cn_get_threshold (neuron_t *neuron);
float cn_get_bias (neuron_t *neuron);
float cn_get_out (neuron_t *neuron);

#endif /* end of include guard: NEURON_H */
