#include <stdio.h>
#include <CUnit/CUnit.h>
#include "../neuron.h"
/*#include "test.h"*/

#define IN 1
#define OUT 1
#define TRS 1.0
#define BIAS 0
#define W 1

void test_zero_inputs (void)
{
	neuron_t n;
	int res;

	res = cn_neuron_new(&n, CN_ACT_STEP, 0, OUT, TRS, BIAS);
	CU_ASSERT_EQUAL(res, -1);
}

void test_zero_outputs (void)
{
	neuron_t n;
	int res;

	res = cn_neuron_new(&n, CN_ACT_STEP, IN, 0, TRS, BIAS);
	CU_ASSERT_EQUAL(res, -1);
}
