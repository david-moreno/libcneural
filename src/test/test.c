#include <stdio.h>
#include <stdlib.h>

#include "../neuron.h"

void neuron_print (neuron_t *neuron)
{
	int i;

	printf("* Activation type: %i\n", neuron->activation_type);
	printf("* Threshold: %f\n", neuron->threshold);
	printf("* Bias: %f\n", neuron->bias);
	printf("* Sum: %f\n", neuron->sum);
	printf("* Output: %f\n", neuron->out);
	putchar('\n');

	printf("Inputs (number, value, weigth):\n");
	for (i=0; i < neuron->input_n; i++) {
		printf("\t%i: %f, %f\n", i, neuron->input[i].value, neuron->input[i].weigth);
	}
	putchar('\n');

	printf("Outputs (number, value, weigth):\n");
	for (i=0; i < neuron->output_n; i++) {
		printf("\t%i: %f, %f\n", i, neuron->output[i].value, neuron->output[i].weigth);
	}
	putchar('\n');
}
