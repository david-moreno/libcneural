#include <stdio.h>
#include <CUnit/Basic.h>
/*#include <CUnit/Automated.h>*/
/*#include "test.h"*/
#include "unit-tests.h"

int main (int argc, const char **argv)
{
	CU_ErrorCode cu_error;

	CU_TestInfo tests1[] = {
		{"Zero inputs", test_zero_inputs},
		{"Zero outputs", test_zero_outputs},
		CU_TEST_INFO_NULL
	};

	CU_SuiteInfo suite1[] = {
		{"Neuron basic functionality", NULL, NULL, tests1},
		CU_SUITE_INFO_NULL
	};

	cu_error = CU_initialize_registry();
	if (cu_error != CUE_SUCCESS) {
		printf("ERROR: Can't start CUnit\n");
		return 1;
	}

	/* CU_register_nsuites() is even more compact:
	 * CU_ErrorCode CU_register_nsuites(int suite_count, ...) */
	cu_error = CU_register_suites(suite1);
	if (cu_error != CUE_SUCCESS) {
		CU_cleanup_registry();
		printf("ERROR: Can't add a suite\n");
		return 2;
	}

	/*CU_automated_run_tests();
	CU_list_tests_to_file();*/

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_basic_show_failures(CU_get_failure_list());
	putchar('\n');

	CU_cleanup_registry();

	return 0;
}
