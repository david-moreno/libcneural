#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "neuron.h"

float cn_activation_step (neuron_t *neuron);
float cn_activation_linear (neuron_t *neuron);
float cn_activation_sigmoid (neuron_t *neuron);
float cn_activation_relu (neuron_t *neuron);
float cn_activation_relu_leaky (neuron_t *neuron);

float cn_neuron_sum (neuron_t *neuron)
{
	int i;
	float sum=0, v, w, output;

	for (i=0; i < neuron->input_n; i++) {
		v = neuron->input[i].value;
		w = neuron->input[i].weigth;
		sum += v * w;
	}

	output = sum + neuron->bias;
	neuron->sum = output;

	for (i=0; i < neuron->output_n; i++) {
		neuron->output[i].value = output;
	}
	
	return output;
}

float (*cn_get_activation (cn_activation_t type)) (neuron_t *)
{
	switch (type) {
		case CN_ACT_STEP: return cn_activation_step; break;
		case CN_ACT_LINEAR: return cn_activation_linear; break;
		case CN_ACT_SIGMOID: return cn_activation_sigmoid; break;
		case CN_ACT_RELU: return cn_activation_relu; break;
		case CN_ACT_RELU_LEAKY: return cn_activation_relu_leaky; break;
		default: return cn_activation_relu; break;
	}
}

/* ==========================================================================
 * Activation functions
 * ========================================================================== */

float cn_activation_step (neuron_t *neuron)
{
	float output = (neuron->sum > neuron->threshold) ? neuron->sum : 0;

	neuron->out = output;

	return output;
}

float cn_activation_linear (neuron_t *neuron)
{
	neuron->out = neuron->sum;
	return neuron->sum;
}

float cn_activation_sigmoid (neuron_t *neuron)
{
	return 0;
}

float cn_activation_relu (neuron_t *neuron)
{
	return 0;
}

float cn_activation_relu_leaky (neuron_t *neuron)
{
	return 0;
}

/* ==========================================================================
 * Constructors
 * ========================================================================== */

int cn_neuron_new (neuron_t *new, cn_activation_t type, unsigned int in, unsigned int out, float trs, float bias)
{
	if (in < 1 || out < 1) return -1;

	new->input = calloc(in, sizeof(connection_t));
	if (new->input == NULL) return -1;

	new->output = calloc(out, sizeof(connection_t));
	if (new->output == NULL) {
		free(new->input);
		return -1;
	}

	new->activation = cn_get_activation(type);
	new->activation_type = type;
	new->input_n = in;
	new->output_n = out;
	new->threshold = trs;
	new->bias = bias;
	new->out = out;

	return type;
}

neuron_t *cn_neuron_array_new (unsigned int num, cn_activation_t type, unsigned int in, unsigned int out, float trs, float bias)
{
	neuron_t *new;
	unsigned int i, j, res;

	if (num < 1 || in < 1 || out < 1) return NULL;

	new = calloc(num, sizeof(neuron_t));
	if (new == NULL) return NULL;

	for (i=0; i < num; i++) {
		res = cn_neuron_new(&new[i], type, in, out, trs, bias);
		if (res = -1) {
			for (j=0; j < i; j++) cn_neuron_free(&new[j]);
			free(new);
			return NULL;
		}
	}

	return new;
}

/* ==========================================================================
 * Destructors
 * ========================================================================== */

void cn_neuron_free (neuron_t *neuron)
{
	if (neuron == NULL) return;
	if (neuron->input != NULL) free(neuron->input);
	if (neuron->output != NULL) free(neuron->output);
}

void cn_neuron_array_free (neuron_t *layer, unsigned int num)
{
	unsigned int i;

	if (layer == NULL) return;
	for (i=0; i < num; i++) cn_neuron_free(&layer[i]);
}

/* ==========================================================================
 * Getters
 * ========================================================================== */
unsigned int cn_get_input_num (neuron_t *neuron)
{
	return neuron->input_n;
}

unsigned int cn_get_output_num (neuron_t *neuron)
{
	return neuron->output_n;
}

cn_activation_t cn_get_activation_type (neuron_t *neuron)
{
	return neuron->activation_type;
}

float cn_get_sum (neuron_t *neuron)
{
	return neuron->sum;
}

float cn_get_threshold (neuron_t *neuron)
{
	return neuron->threshold;
}

float cn_get_bias (neuron_t *neuron)
{
	return neuron->bias;
}

float cn_get_out (neuron_t *neuron)
{
	return neuron->out;
}
