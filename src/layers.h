#ifndef LAYERS_H
#define LAYERS_H

typedef struct cn_layer_t cn_layer_t;

cn_layer_t *cn_layer_new (unsigned int num, cn_activation_t type, unsigned int in, unsigned int out, float trs, float bias);
void cn_layer_free (cn_layer_t *obj);

#endif /* end of include guard: LAYERS_H */
