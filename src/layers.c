#include <stdio.h>
#include <stdlib.h>
#include "neuron.h"
#include "layers.h"

typedef struct cn_layer_t {
	unsigned int num;
	neuron_t *neuron;
} cn_layer_t;

cn_layer_t *cn_layer_new (unsigned int num, cn_activation_t type, unsigned int in, unsigned int out, float trs, float bias)
{
	cn_layer_t *new;
	neuron_t *array;

	if (num < 1) return NULL;

	new = calloc(1, sizeof(cn_layer_t));
	if (new == NULL) return NULL;

	array = cn_neuron_array_new(num, type, in, out, trs, bias);
	if (array == NULL) {
		free(new);
		return NULL;
	}

	new->num = num;
	new->neuron = array;

	return new;
}

void cn_layer_free (cn_layer_t *obj)
{
	if (obj == NULL) return;
	if (obj->neuron != NULL) cn_neuron_layer_free(obj->neuron);
	free(obj);
}
